cmake_minimum_required(VERSION 2.8.3)
project(my_empty)


find_package(
  catkin REQUIRED COMPONENTS
    roscpp
)

catkin_package(
  # INCLUDE_DIRS include
)


# add_compile_options(-std=c++11)
# SET(CMAKE_CXX_FLAGS "-O2 -g -Wall ${CMAKE_CXX_FLAGS}")
# set(ROSLINT_CPP_OPTS "--filter=-build/c++11")
# roslint_cpp()

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable(
  ${PROJECT_NAME}_node
  src/my_empty_node.cpp
)

add_dependencies(${PROJECT_NAME}_node ${catkin_EXPORTED_TARGETS}) # 检查依赖
target_link_libraries(${PROJECT_NAME}_node ${catkin_LIBRARIES})



##############################################
#   INSTALL
##############################################

install(
  TARGETS ${PROJECT_NAME}_node
  #ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  #LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
