/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PURE_PURSUIT_PURE_PURSUIT_CORE_H
#define PURE_PURSUIT_PURE_PURSUIT_CORE_H

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <gps_common/GPSFix.h>

// #include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>


#include <vector>
#include <memory>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PointStamped.h> 
#include <tf/transform_listener.h>

#include <geometry_msgs/Pose.h>

//  Geocentric
#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/LocalCartesian.hpp>
using namespace GeographicLib;


#include <cmath>
#define RAD2DEG  (180.0/M_PI)

using namespace std;

namespace gnss2tfpose 
{

enum class Mode : int32_t
{
  waypoint,
  dialog,
  unknown = -1,
};

template <class T>
typename std::underlying_type<T>::type enumToInteger(T t)
{
  return static_cast<typename std::underlying_type<T>::type>(t);
}


class Gnss2TFPoseNode

{
public:
  Gnss2TFPoseNode();
  ~Gnss2TFPoseNode();

  void run();
  // friend class PurePursuitNodeTestSuite;

private:
  // handle
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

  // publisher
  ros::Publisher pub1_,pub2_,pub3_, pub4_;

  // subscriber
  ros::Subscriber sub1_, sub2_;

  geometry_msgs::TwistStamped gnss_twist;

  // constant
  const int LOOP_RATE_;  // processing frequency
  const std::string MAP_FRAME_;  // processing frequency
  const std::string GPS_FRAME_;  // processing frequency
  tf::TransformListener *p_listener;
  
  //  Geocentric
  LocalCartesian * p_local;

  tf::TransformBroadcaster broadcaster;

  geometry_msgs::Pose pose_;
  double roll_, pitch_, yaw_;

  geometry_msgs::PoseStamped pose_ndt_gnss; 

  std::string estimate_twist;
  double high_of_base_link;

  double localcartesian_lat0_; // 坐标原点值
  double localcartesian_lon0_;
  double localcartesian_h0_;
  double localcartesian_yaw_;

  // initializer
  void initForROS();


  // callbacks

  void callbackFromgnss_twist( const geometry_msgs::TwistStamped& twist);
  void callbackFromGnssinfo( const gps_common::GPSFix& gnss_fix);



  void publishPoseStamped();
  void publishPoseStamped2();
  // void publishTF();
};


}  // namespace waypoint_follower

#endif  // PURE_PURSUIT_PURE_PURSUIT_CORE_H
