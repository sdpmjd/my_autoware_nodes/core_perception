/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <ars_40x/ObjectList.h>
#include <autoware_msgs/DetectedObjectArray.h>

// #include "my_autoware_config_msgs/ConfigVoxelGridFilter.h"
// #include <my_points_downsampler/PointsDownsamplerInfo.h>

// #include <chrono>

#include "ars_40x_to_autoware_object.h"

// #define MAX_MEASUREMENT_RANGE 200.0

ros::Publisher radar_font_objects_pub, radar_font_cloud_clusters_pub;

static bool _output_log = false;

static std::string POINTS_TOPIC;

static void create_pcl_pointcloud(const autoware_msgs::DetectedObject &detected_object,
                                  pcl::PointCloud<pcl::PointXYZI> &pcl_radar_points)
{
  // sensor_msgs::PointCloud2

  for (int dx = -50; dx <= 50; dx+=10){
    for (int dy = -50; dy <= 50; dy+=25){
      for (int dz = 0; dz <= 100; dz+=25){

        pcl::PointXYZI point;
        point.x = detected_object.pose.position.x + dx/100.0;
        point.y = detected_object.pose.position.y + dy/100.0;
        point.z = detected_object.pose.position.z + dz/100.0;
        pcl_radar_points.points.push_back(point);
      }
    }
  }
}


static void ars_40x_objects_callback(const ars_40x::ObjectList::ConstPtr& ars_40x_objectlist)
{

  pcl::PointCloud<pcl::PointXYZI> pcl_radar_points;
  autoware_msgs::DetectedObjectArray detected_objectarray;
  for (auto ars_40x_obj: ars_40x_objectlist->objects)
  {
      // ROS_INFO_STREAM(ars_40x_obj.position.pose); 
      // ROS_INFO_STREAM(ars_40x_obj.relative_velocity.twist); 
      // ROS_INFO_STREAM(ars_40x_obj.relative_acceleration.accel); 
      // ROS_INFO_STREAM(ars_40x_obj.length); 
      // ROS_INFO_STREAM(ars_40x_obj.width); 
      // ROS_INFO_STREAM(ars_40x_obj.orientation_angle); 
      // ROS_INFO_STREAM(ars_40x_obj.rcs); 
      // ROS_INFO_STREAM(ars_40x_obj.dynamic_property); 
      // ROS_INFO_STREAM(ars_40x_obj.class_type); 
      // ROS_INFO_STREAM(ars_40x_obj.meas_state); 
      // ROS_INFO_STREAM(ars_40x_obj.prob_of_exist); 

      autoware_msgs::DetectedObject detected_object;
      detected_object.id = ars_40x_obj.id;
      detected_object.pose = ars_40x_obj.position.pose;
      detected_object.velocity = ars_40x_obj.relative_velocity.twist;
      // detected_object.acceleration = ars_40x_obj.relative_acceleration.accel;

      create_pcl_pointcloud(detected_object,pcl_radar_points);

      detected_objectarray.objects.push_back(detected_object);

  }

  detected_objectarray.header.frame_id = "radar";
  detected_objectarray.header.stamp = ros::Time::now();
  radar_font_objects_pub.publish(detected_objectarray);

  sensor_msgs::PointCloud2 radar_points;
  pcl::toROSMsg(pcl_radar_points, radar_points);
  radar_points.header.frame_id="radar";
  radar_points.header.stamp = ros::Time::now();
  radar_font_cloud_clusters_pub.publish(radar_points);

  // pcl::PointCloud<pcl::PointXYZI> scan;
  // scan = removePointsByRange(scan, min_x,max_x, 
  //                                    min_y,max_y, 
  //                                    min_z,max_z );
  //convert

  // pcl::PointCloud<pcl::PointXYZI>::Ptr scan_ptr(new pcl::PointCloud<pcl::PointXYZI>(scan));
  // sensor_msgs::PointCloud2 filtered_msg;
  // pcl::toROSMsg(*scan_ptr, filtered_msg);

  // filtered_msg.header = input->header;
  // filtered_msg.header.frame_id="velodyne";
  // filtered_points_pub.publish(filtered_msg);

  // if(_output_log == true){
	//   // std::cout << "," << std::endl;
  // }



}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "voxel_grid_filter");

  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  // private_nh.getParam("points_topic", POINTS_TOPIC);
  // private_nh.getParam("output_log", _output_log);

  // Publishers
  radar_font_objects_pub = nh.advertise<autoware_msgs::DetectedObjectArray>("/detection/radar_detector/objects", 10);
  radar_font_cloud_clusters_pub = nh.advertise<sensor_msgs::PointCloud2>("/detection/radar_detector/cloud_clusters", 10);

  ros::Subscriber config_sub = nh.subscribe("ars_40x/objects", 10, ars_40x_objects_callback);
  // ros::Subscriber scan_sub = nh.subscribe(POINTS_TOPIC, 10, scan_callback);

  ros::spin();

  return 0;
}
