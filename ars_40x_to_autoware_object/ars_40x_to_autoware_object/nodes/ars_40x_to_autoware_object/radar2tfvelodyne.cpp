/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <ars_40x/ObjectList.h>
#include <autoware_msgs/DetectedObjectArray.h>

#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PointStamped.h> 
#include <tf/transform_listener.h>

#include <math.h>
#include "ars_40x_to_autoware_object.h"


ros::Publisher radar_font_objects_pub, radar_font_cloud_clusters_pub;

static bool _output_log = false;

static std::string POINTS_TOPIC;

tf::TransformListener *p_listener;


static void create_pcl_pointcloud(autoware_msgs::DetectedObject &detected_object,
                                  pcl::PointCloud<pcl::PointXYZI> &pcl_radar_points)
{
  // sensor_msgs::PointCloud2
// 
  for (int dx = -25; dx <= 25; dx+=5){
    for (int dy = -25; dy <= 25; dy+=5){
      for (int dz = 0; dz <= 50; dz+=5){

        pcl::PointXYZI point;
        point.x = detected_object.pose.position.x + dx/100.0;
        point.y = detected_object.pose.position.y + dy/100.0;
        point.z = detected_object.pose.position.z + dz/100.0;
        pcl_radar_points.points.push_back(point);

        // convex_hull
        if (fabs(dx)==25 && fabs(dy)==25 && (dz==0 || dz==50)){
          geometry_msgs::Point32 point32;
          point32.x =point.x; 
          point32.y =point.y;
          point32.z =point.z;
          detected_object.convex_hull.polygon.points.push_back(point32);
        }
      }
    }
  }

}


static void ars_40x_objects_callback(const ars_40x::ObjectList::ConstPtr& ars_40x_objectlist)
{

  pcl::PointCloud<pcl::PointXYZI> pcl_radar_points;
  autoware_msgs::DetectedObjectArray detected_objectarray;
  for (auto ars_40x_obj: ars_40x_objectlist->objects)
  {
      // ROS_INFO_STREAM(ars_40x_obj.position.pose); 
      // ROS_INFO_STREAM(ars_40x_obj.relative_velocity.twist); 
      // ROS_INFO_STREAM(ars_40x_obj.relative_acceleration.accel); 
      // ROS_INFO_STREAM(ars_40x_obj.length); 
      // ROS_INFO_STREAM(ars_40x_obj.width); 
      // ROS_INFO_STREAM(ars_40x_obj.orientation_angle); 
      // ROS_INFO_STREAM(ars_40x_obj.rcs); 
      // ROS_INFO_STREAM(ars_40x_obj.dynamic_property); 
      // ROS_INFO_STREAM(ars_40x_obj.class_type); 
      // ROS_INFO_STREAM(ars_40x_obj.meas_state); 
      // ROS_INFO_STREAM(ars_40x_obj.prob_of_exist); 

      autoware_msgs::DetectedObject detected_object;
      detected_object.id = ars_40x_obj.id;

     {
        geometry_msgs::PoseStamped radar_posestamped,velodyne_pose;
        radar_posestamped.header.frame_id = "radar"; //MAP_FRAME_;
        radar_posestamped.header.stamp = ros::Time();
        radar_posestamped.pose= ars_40x_obj.position.pose;
        // radar_posestamped.pose.position.y = 0;
        // radar_posestamped.pose.position.z = 0;
        // radar_posestamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, 0);

        // 将radar 下的坐标　转换到 velodyne 下
        p_listener->transformPose("velodyne", radar_posestamped, velodyne_pose);
        detected_object.pose = velodyne_pose.pose;

       }
       detected_object.velocity = ars_40x_obj.relative_velocity.twist;
       // detected_object.acceleration = ars_40x_obj.relative_acceleration.accel;

       create_pcl_pointcloud(detected_object,pcl_radar_points);

       detected_objectarray.objects.push_back(detected_object);

  }

  detected_objectarray.header.frame_id = "velodyne";
  detected_objectarray.header.stamp = ros::Time::now();
  radar_font_objects_pub.publish(detected_objectarray);


  sensor_msgs::PointCloud2 radar_points;
  pcl::toROSMsg(pcl_radar_points, radar_points);
  radar_points.header.frame_id="velodyne";
  radar_points.header.stamp = ros::Time::now();
  radar_font_cloud_clusters_pub.publish(radar_points);


  // pcl::PointCloud<pcl::PointXYZI>::Ptr scan_ptr(new pcl::PointCloud<pcl::PointXYZI>(scan));
  // sensor_msgs::PointCloud2 filtered_msg;
  // pcl::toROSMsg(*scan_ptr, filtered_msg);

  // filtered_msg.header = input->header;
  // filtered_msg.header.frame_id="velodyne";
  // filtered_points_pub.publish(filtered_msg);

  // if(_output_log == true){
	//   // std::cout << "," << std::endl;
  // }

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "voxel_grid_filter");

  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  // private_nh.getParam("points_topic", POINTS_TOPIC);
  // private_nh.getParam("output_log", _output_log);

  p_listener = new tf::TransformListener(ros::Duration(10));

  // Publishers

  radar_font_objects_pub = nh.advertise<autoware_msgs::DetectedObjectArray>("/detection/lidar_detector/objects", 10);
  radar_font_cloud_clusters_pub = nh.advertise<sensor_msgs::PointCloud2>("/detection/lidar_detector/cloud_clusters", 10);

  ros::Subscriber config_sub = nh.subscribe("ars_40x/objects", 10, ars_40x_objects_callback);
  // ros::Subscriber scan_sub = nh.subscribe(POINTS_TOPIC, 10, scan_callback);

  ros::spin();

  delete  p_listener;

  return 0;
}
