#ifndef POINTS_DOWNSAMPLER_H
#define POINTS_DOWNSAMPLER_H

#if 0
static pcl::PointCloud<pcl::PointXYZI> removePointsByRange(
                                                pcl::PointCloud<pcl::PointXYZI> scan, 
                                                double min_x = -100000, 
                                                double max_x = 1000000, 
                                                double min_y = -100000, 
                                                double max_y = 100000, 
                                                double min_z = -100000, 
                                                double max_z = 1000000 )
{
  pcl::PointCloud<pcl::PointXYZI> narrowed_scan;
  narrowed_scan.header = scan.header;

  for(pcl::PointCloud<pcl::PointXYZI>::const_iterator iter = scan.begin(); iter != scan.end(); ++iter)
  {
    const pcl::PointXYZI &p = *iter;

#if 1     //  This error handling should be detemind.
    if( min_x>=max_x ) {
      ROS_ERROR_ONCE("min_x >= max_x @(%lf, %lf)", min_x, max_x );
    }
    if( min_y>=max_y ) {
      ROS_ERROR_ONCE("min_y >= max_y @(%lf, %lf)", min_y, max_y );
    }
    if( min_z>=max_z ) {
      ROS_ERROR_ONCE("min_z >= max_z @(%lf, %lf)", min_z, max_z );
    }
#endif

    if  ((p.x > min_x && p.x <  max_x ) && \
         (p.y > min_y && p.y <  max_y ) && \
         (p.z > min_z && p.z <  max_z ) )
    {
        continue;
    }
    narrowed_scan.points.push_back(p);
  }
  return narrowed_scan;
}

#endif
#endif // POINTS_DOWNSAMPLER_H
